# Deep Dive into Docker

###### by @rawkode

------

## Docker Fundamentals

- [Creating Containers, Part I](1-fundamentals/1.1-Creating Containers, Part I.md)
- [Official Images](1-fundamentals/1.2-Official Images.md)
- [Creating Containers, Part II](1-fundamentals/1.3-Creating Containers, Part II.md)
- [Building Images, Part I](1-fundamentals/1.4-Building Images, Part I.md)
- [Building Images, Part II](1-fundamentals/1.5-Building Images, Part II.md)
- [Publishing images](1-fundamentals/1.6-Publishing Images.md)

## Docker Orchestration in Development

- [Docker Compose](3-orchestration-in-development/3.1-docker-compose.md)

## Advanced Docker

- Optimising Images
- Networking
- Volumes
- Events

## Docker Orchestration in Production

- Swarm Mode

